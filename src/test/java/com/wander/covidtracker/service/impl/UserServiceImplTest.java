package com.wander.covidtracker.service.impl;

import com.wander.covidtracker.model.Role;
import com.wander.covidtracker.model.User;
import com.wander.covidtracker.repository.RoleRepository;
import com.wander.covidtracker.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserServiceImplTest {

    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private RoleRepository mockRoleRepository;
    @Mock
    private BCryptPasswordEncoder mockBCryptPasswordEncoder;

    @InjectMocks
    private UserServiceImpl userServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void testSave() {
        // Setup
        final User user = new User();
        user.setId(0L);
        user.setEmail("username");
        user.setPassword("password");
        user.setPasswordConfirm("passwordConfirm");
        final Role role = new Role();
        role.setId(0L);
        role.setName("name");
        role.setUsers(new HashSet<>(Arrays.asList(new User())));
        user.setRoles(new HashSet<>(Arrays.asList(role)));
        user.setFullName("fullName");

        when(mockBCryptPasswordEncoder.encode("rawPassword")).thenReturn("result");

        // Configure RoleRepository.findAll(...).
        final Role role1 = new Role();
        role1.setId(0L);
        role1.setName("name");
        final User user1 = new User();
        user1.setId(0L);
        user1.setEmail("username");
        user1.setPassword("password");
        user1.setPasswordConfirm("passwordConfirm");
        user1.setRoles(new HashSet<>(Arrays.asList(new Role())));
        user1.setFullName("fullName");
        role1.setUsers(new HashSet<>(Arrays.asList(user1)));
        final List<Role> roles = Arrays.asList(role1);
        when(mockRoleRepository.findAll()).thenReturn(roles);

        // Configure UserRepository.save(...).
        final User user2 = new User();
        user2.setId(0L);
        user2.setEmail("username");
        user2.setPassword("password");
        user2.setPasswordConfirm("passwordConfirm");
        final Role role2 = new Role();
        role2.setId(0L);
        role2.setName("name");
        role2.setUsers(new HashSet<>(Arrays.asList(new User())));
        user2.setRoles(new HashSet<>(Arrays.asList(role2)));
        user2.setFullName("fullName");
        when(mockUserRepository.save(any(User.class))).thenReturn(user2);

        // Run the test
        userServiceImplUnderTest.save(user);

        // Verify the results
    }

    @Test
    void testFindByUsername() {
        // Setup

        // Configure UserRepository.findByEmail(...).
        final User user = new User();
        user.setId(0L);
        user.setEmail("username");
        user.setPassword("password");
        user.setPasswordConfirm("passwordConfirm");
        final Role role = new Role();
        role.setId(0L);
        role.setName("name");
        role.setUsers(new HashSet<>(Arrays.asList(new User())));
        user.setRoles(new HashSet<>(Arrays.asList(role)));
        user.setFullName("fullName");
        when(mockUserRepository.findByEmail("username")).thenReturn(user);

        // Run the test
        final User result = userServiceImplUnderTest.findByUsername("username");

        // Verify the results

        assertThat(result).isEqualTo(user);
    }

    @Test
    void testLoadUserByUsername() {
        // Setup

        // Configure UserRepository.findByEmail(...).
        final User user = new User();
        user.setId(0L);
        user.setEmail("email");
        user.setPassword("password");
        user.setPasswordConfirm("passwordConfirm");
        final Role role = new Role();
        role.setId(0L);
        role.setName("name");
        role.setUsers(new HashSet<>(Arrays.asList(new User())));
        user.setRoles(new HashSet<>(Arrays.asList(role)));
        user.setFullName("fullName");
        when(mockUserRepository.findByEmail("email")).thenReturn(user);

        // Run the test
        final UserDetails result = userServiceImplUnderTest.loadUserByUsername("email");

        // Verify the results
    }
}

package com.wander.covidtracker.service.impl;

import com.wander.covidtracker.model.*;
import com.wander.covidtracker.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class CovidDataServiceImplTest {

    @Mock
    private CountryDataRepository mockCountryDataRepo;
    @Mock
    private StateDataRepository mockStateDataRepo;
    @Mock
    private CityDataRepository mockCityDataRepo;
    @Mock
    private CountryDailyDataRepository mockCountryDailyDataRepo;
    @Mock
    private CityDailyDataRepository mockCityDailyDataRepo;
    @Mock
    private StateDailyDataRepository mockStateDailyDataRepo;
    @Mock
    private CountryRepository mockCountryRepo;
    @Mock
    private StateRepository mockStateRepo;
    @Mock
    private CityRepository mockCityRepo;

    @InjectMocks
    private CovidDataServiceImpl covidDataServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void testGetCountryData() {
        // Setup

        // Configure CountryDataRepository.findById(...).
        Country country = new Country("code", "name");
        CountryData data = new CountryData(new CountryData.DataId(country));
        final Optional<CountryData> countryData = Optional
                .of(data);
        when(mockCountryDataRepo.findById(new CountryData.DataId(country))).thenReturn(countryData);

        when(mockCountryRepo.findById("code")).thenReturn(Optional.of(country));

        // Run the test
        final CountryData result = covidDataServiceImplUnderTest.getCountryData("code");

        // Verify the results

        assertThat(result).isEqualTo(data);
    }

    @Test
    void testGetStateData() {
        // Setup

        // Configure CountryDataRepository.findById(...).
        Country country = new Country("code", "name");
        final Optional<CountryData> countryData = Optional.of(new CountryData(new CountryData.DataId(country)));
        when(mockCountryDataRepo.findById(new CountryData.DataId(country))).thenReturn(countryData);

        when(mockCountryRepo.findById("code")).thenReturn(Optional.of(country));

        // Run the test
        final List<StateData> result = covidDataServiceImplUnderTest.getStateData("code");

        Assertions.assertNull(result);

        // Verify the results
    }

    @Test
    void testGetStateData1() {
        // Setup
        Country country = new Country("countryCode", "name");
        when(mockCountryRepo.findById("countryCode")).thenReturn(Optional.of(country));
        State.StateId stateId = new State.StateId("stateCode", country);
        State state = new State(stateId);
        when(mockStateRepo.findById(stateId)).thenReturn(Optional.of(state));

        // Configure StateDataRepository.findById(...).
        StateData.DataId dataId = new StateData.DataId(state);
        StateData data = new StateData(dataId);
        final Optional<StateData> stateData = Optional.of(data);
        when(mockStateDataRepo.findById(dataId)).thenReturn(stateData);

        // Run the test
        final StateData result = covidDataServiceImplUnderTest.getStateData("countryCode", "stateCode");

        assertThat(result).isEqualTo(data);
        // Verify the results
    }

    @Test
    void testGetCityData() {
        // Setup
        Country country = new Country("country", "name");
        when(mockCountryRepo.findById("country")).thenReturn(Optional.of(country));
        State.StateId stateId = new State.StateId("state", country);
        State state = new State(stateId);
        when(mockStateRepo.findById(stateId)).thenReturn(Optional.of(state));

        // Configure StateDataRepository.findById(...).
        StateData data = new StateData(new StateData.DataId(state));
        final Optional<StateData> stateData = Optional.of(data);
        when(mockStateDataRepo.findById(new StateData.DataId(state))).thenReturn(stateData);

        // Run the test
        final List<CityData> result = covidDataServiceImplUnderTest.getCityData("country", "state");

        // Verify the results
        Assertions.assertNull(result);
    }

    @Test
    void testGetStateDailyData() {
        // Setup

        // Configure CountryDailyDataRepository.findAllByCountry(...).
        final CountryDailyData countryDailyData1 = new CountryDailyData();
        countryDailyData1.setCountry("country");
        final StateDailyData stateDailyData = new StateDailyData();
        stateDailyData.setCountry("country");
        stateDailyData.setState("state");
        final CityDailyData cityDailyData = new CityDailyData();
        cityDailyData.setCountry("country");
        cityDailyData.setState("state");
        cityDailyData.setCity("city");
        stateDailyData.setCityDailyData(Arrays.asList(cityDailyData));
        countryDailyData1.setStateDailyData(Arrays.asList(stateDailyData));
        final List<CountryDailyData> countryDailyData = Arrays.asList(countryDailyData1);
        when(mockCountryDailyDataRepo.findAllByCountry("country")).thenReturn(countryDailyData);

        // Run the test
        final List<CountryDailyData> result = covidDataServiceImplUnderTest.getStateDailyData("countryCode");

        // Verify the results
    }

    @Test
    void testGetStateDailyData1() {
        // Setup

        // Configure StateDailyDataRepository.findAllByCountryAndState(...).
        final StateDailyData stateDailyData1 = new StateDailyData();
        stateDailyData1.setCountry("country");
        stateDailyData1.setState("state");
        final CityDailyData cityDailyData = new CityDailyData();
        cityDailyData.setCountry("country");
        cityDailyData.setState("state");
        cityDailyData.setCity("city");
        stateDailyData1.setCityDailyData(Arrays.asList(cityDailyData));
        final List<StateDailyData> stateDailyData = Arrays.asList(stateDailyData1);
        when(mockStateDailyDataRepo.findAllByCountryAndState("country", "state")).thenReturn(stateDailyData);

        // Run the test
        final List<StateDailyData> result = covidDataServiceImplUnderTest.getStateDailyData("country", "state");

        // Verify the results
        assertThat(result).isEqualTo(stateDailyData);
    }

    @Test
    void testGetCityDailyData() {
        // Setup

        // Configure CityDailyDataRepository.findAllByCountryAndStateAndCity(...).
        final CityDailyData cityDailyData1 = new CityDailyData();
        cityDailyData1.setCountry("country");
        cityDailyData1.setState("state");
        cityDailyData1.setCity("city");
        final List<CityDailyData> cityDailyData = Arrays.asList(cityDailyData1);
        when(mockCityDailyDataRepo.findAllByCountryAndStateAndCity("country", "state", "city"))
                .thenReturn(cityDailyData);

        // Run the test
        final List<CityDailyData> result = covidDataServiceImplUnderTest
                .getCityDailyData("country", "state", "city");

        // Verify the results
    }

    @Test
    void testGetCityData1() {
        // Setup
        Country country = new Country("country", "name");
        when(mockCountryRepo.findById("country")).thenReturn(Optional.of(country));
        State.StateId stateId = new State.StateId("state", country);
        State state = new State(stateId);
        when(mockStateRepo.findById(stateId)).thenReturn(Optional.of(state));
        City.CityId cityId = new City.CityId("city", state);
        City city = new City(cityId);
        when(mockCityRepo.findById(cityId)).thenReturn(Optional.of(city));
        // Configure CityDataRepository.findById(...).
        CityData data = new CityData(new CityData.DataId(city));
        final Optional<CityData> cityData = Optional.of(data);
        when(mockCityDataRepo.findById(new CityData.DataId(city))).thenReturn(cityData);

        // Run the test
        final CityData result = covidDataServiceImplUnderTest.getCityData("country", "state", "city");

        // Verify the results

        assertThat(result).isEqualTo(data);
    }

    @Test
    void testGetMeanValue() {
        // Setup
        PageRequest of = PageRequest.of(0, 1);
        when(mockCountryDailyDataRepo.getConfirmedForNdays(anyString(), any(PageRequest.class)))
                .thenReturn(Arrays.asList(0L));

        // Run the test
        final double result = covidDataServiceImplUnderTest.getMeanValue("countryCode", 1);

        // Verify the results
        assertThat(result).isEqualTo(0.0, within(0.0001));
    }

    @Test
    void testGetCountryDataLastNdays() {
        // Setup

        // Configure CountryDailyDataRepository.findByCountry(...).
        final CountryDailyData countryDailyData1 = new CountryDailyData();
        countryDailyData1.setCountry("country");
        final StateDailyData stateDailyData = new StateDailyData();
        stateDailyData.setCountry("country");
        stateDailyData.setState("state");
        final CityDailyData cityDailyData = new CityDailyData();
        cityDailyData.setCountry("country");
        cityDailyData.setState("state");
        cityDailyData.setCity("city");
        stateDailyData.setCityDailyData(Arrays.asList(cityDailyData));
        countryDailyData1.setStateDailyData(Arrays.asList(stateDailyData));
        final List<CountryDailyData> countryDailyData = Arrays.asList(countryDailyData1);
        when(mockCountryDailyDataRepo.findByCountry("countryCode", PageRequest.of(0, 1))).thenReturn(countryDailyData);

        // Run the test
        final List<CountryDailyData> result = covidDataServiceImplUnderTest.getCountryDataLastNdays("countryCode", 0);

        // Verify the results
    }

    @Test
    void testGetCountry() {
        // Setup
        Country country = new Country("countryCode", "name");
        when(mockCountryRepo.findById("countryCode")).thenReturn(Optional.of(country));

        // Run the test
        final Country result = covidDataServiceImplUnderTest.getCountry("countryCode");

        // Verify the results
        assertThat(country).isEqualTo(result);

    }

    @Test
    void testGetState() {
        // Setup
        Country country = new Country("countryCode", "name");
        when(mockCountryRepo.findById("countryCode")).thenReturn(Optional.of(country));
        when(mockStateRepo.findById(new State.StateId("stateCode", country)))
                .thenReturn(Optional.of(new State()));

        // Run the test
        final State result = covidDataServiceImplUnderTest.getState("countryCode", "stateCode");

        // Verify the results

        assertThat(result).isEqualTo(result);
    }

    @Test
    void testGetCity() {
        // Setup
        Country country = new Country("countryCode", "name");
        when(mockCountryRepo.findById("countryCode")).thenReturn(Optional.of(country));
        State state = new State();
        when(mockStateRepo.findById(new State.StateId("stateCode", country))).thenReturn(Optional.of(state));
        City city = new City();
        when(mockCityRepo.findById(new City.CityId("cityName", state))).thenReturn(Optional.of(city));

        // Run the test
        final City result = covidDataServiceImplUnderTest.getCity("countryCode", "stateCode", "cityName");
        assertThat(city).isEqualTo(result);
        // Verify the results
    }
}

package com.wander.covidtracker;

import com.wander.covidtracker.model.Country;
import com.wander.covidtracker.service.DataSyncService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class TestConfig {
    @Bean
    public DataSyncService dataSyncService() {
        return new DataSyncService(){

            @Override
            public void syncDailyData() {

            }

            @Override
            public void syncLatestData() {

            }

            @Override
            public void syncDailyData(Country country) throws IOException {

            }
        };
    }
}

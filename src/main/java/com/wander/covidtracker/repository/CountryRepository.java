package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Country repository.
 */
public interface CountryRepository extends JpaRepository<Country, String> {
}

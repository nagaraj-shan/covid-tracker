package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.StateData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface State data repository.
 */
public interface StateDataRepository extends JpaRepository<StateData, StateData.DataId> {

}

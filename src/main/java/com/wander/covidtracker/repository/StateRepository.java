package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.State;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface State repository.
 */
public interface StateRepository extends JpaRepository<State, State.StateId> {

}

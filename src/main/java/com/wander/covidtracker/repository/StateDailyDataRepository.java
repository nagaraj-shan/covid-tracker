package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.StateDailyData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * The interface State daily data repository.
 */
public interface StateDailyDataRepository extends JpaRepository<StateDailyData, Long> {

    /**
     * Find by date and country and state optional.
     *
     * @param date    the date
     * @param country the country
     * @param state   the state
     * @return the optional
     */
    Optional<StateDailyData> findByDateAndCountryAndState(LocalDate date, String country, String state);

    /**
     * Find all by country and state list.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @return the list
     */
    List<StateDailyData> findAllByCountryAndState(String countryCode, String stateCode);
}

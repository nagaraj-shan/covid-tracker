package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.CountryData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Country data repository.
 */
public interface CountryDataRepository extends JpaRepository<CountryData, CountryData.DataId> {

}

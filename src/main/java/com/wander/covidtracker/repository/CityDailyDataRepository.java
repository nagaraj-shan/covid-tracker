package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.CityDailyData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * The interface City daily data repository.
 */
public interface CityDailyDataRepository extends JpaRepository<CityDailyData, Long> {

    /**
     * Find by date and country and state and city optional.
     *
     * @param date    the date
     * @param country the country
     * @param state   the state
     * @param city    the city
     * @return the optional
     */
    Optional<CityDailyData> findByDateAndCountryAndStateAndCity(LocalDate date, String country, String state, String city);

    /**
     * Find all by country and state and city list.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @param cityName    the city name
     * @return the list
     */
    List<CityDailyData> findAllByCountryAndStateAndCity(String countryCode, String stateCode, String cityName);
}

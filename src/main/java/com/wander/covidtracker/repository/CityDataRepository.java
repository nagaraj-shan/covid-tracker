package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.CityData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface City data repository.
 */
public interface CityDataRepository extends JpaRepository<CityData, CityData.DataId> {
}

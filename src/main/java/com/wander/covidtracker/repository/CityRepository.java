package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface City repository.
 */
public interface CityRepository extends JpaRepository<City, City.CityId> {
}

package com.wander.covidtracker.repository;

import com.wander.covidtracker.model.CountryDailyData;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * The interface Country daily data repository.
 */
public interface CountryDailyDataRepository extends JpaRepository<CountryDailyData, Long> {

    /**
     * Last data date optional.
     *
     * @param countryCode the country code
     * @return the optional
     */
    @Query(value = "SELECT max(cd.date) FROM CountryDailyData cd WHERE cd.country=:country")
    Optional<LocalDate> lastDataDate(@Param("country") String countryCode);

    /**
     * Gets confirmed for ndays.
     *
     * @param countryCode the country code
     * @param pageable    the pageable
     * @return the confirmed for ndays
     */
    @Query(value = "SELECT cd.total.confirmed FROM CountryDailyData cd WHERE cd.country=:country")
    List<Long> getConfirmedForNdays(@Param("country") String countryCode, Pageable pageable);

    /**
     * Find by country list.
     *
     * @param countryCode the country code
     * @param pageRequest the page request
     * @return the list
     */
    @Query(value = "SELECT cd FROM CountryDailyData cd WHERE cd.country= ?1")
    List<CountryDailyData> findByCountry(String countryCode, PageRequest pageRequest);

    /**
     * Find by date and country optional.
     *
     * @param date    the date
     * @param country the country
     * @return the optional
     */
    Optional<CountryDailyData> findByDateAndCountry(LocalDate date, String country);

    /**
     * Find all by country list.
     *
     * @param country the country
     * @return the list
     */
    List<CountryDailyData> findAllByCountry(String country);
}

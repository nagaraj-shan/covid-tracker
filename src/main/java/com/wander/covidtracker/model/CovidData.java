package com.wander.covidtracker.model;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.StringJoiner;

/**
 * The type Covid data.
 */
@Embeddable
public class CovidData {
    private long confirmed = 0;
    private long deceased = 0;
    private long recovered = 0;
    private long tested = 0;

    /**
     * Gets confirmed.
     *
     * @return the confirmed
     */
    public long getConfirmed() {
        return confirmed;
    }

    /**
     * Sets confirmed.
     *
     * @param confirmed the confirmed
     */
    public void setConfirmed(long confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * Gets deceased.
     *
     * @return the deceased
     */
    public long getDeceased() {
        return deceased;
    }

    /**
     * Sets deceased.
     *
     * @param deceased the deceased
     */
    public void setDeceased(long deceased) {
        this.deceased = deceased;
    }

    /**
     * Gets recovered.
     *
     * @return the recovered
     */
    public long getRecovered() {
        return recovered;
    }

    /**
     * Sets recovered.
     *
     * @param recovered the recovered
     */
    public void setRecovered(long recovered) {
        this.recovered = recovered;
    }

    /**
     * Gets tested.
     *
     * @return the tested
     */
    public long getTested() {
        return tested;
    }

    /**
     * Sets tested.
     *
     * @param tested the tested
     */
    public void setTested(long tested) {
        this.tested = tested;
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    @Transient
    public long getActive() {
        return confirmed - (recovered + deceased);
    }

    /**
     * Gets active percent.
     *
     * @return the active percent
     */
    @Transient
    public double getActivePercent() {
        return Long.valueOf(getActive()).doubleValue() / Long.valueOf(confirmed).doubleValue() * 100;

    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CovidData.class.getSimpleName() + "[", "]")
                .add("confirmed=" + confirmed)
                .add("deceased=" + deceased)
                .add("recovered=" + recovered)
                .add("tested=" + tested)
                .toString();
    }


}

package com.wander.covidtracker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * The type Country daily data.
 */
@Entity
@Table(name = "country_daily_data")
public class CountryDailyData extends AbstractCovidDailyData {

    @Column
    private String country;

    @JsonIgnore
    @Transient
    private List<StateDailyData> stateDailyData;

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Sets state daily data.
     *
     * @param stateDailyData the state daily data
     */
    public void setStateDailyData(List<StateDailyData> stateDailyData) {
        this.stateDailyData = stateDailyData;
    }

    /**
     * Gets state daily data.
     *
     * @return the state daily data
     */
    public List<StateDailyData> getStateDailyData() {
        return stateDailyData;
    }
}

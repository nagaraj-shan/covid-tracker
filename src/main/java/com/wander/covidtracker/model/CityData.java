package com.wander.covidtracker.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * The type City data.
 */
@Entity
@Table(name = "city_data")
public class CityData extends AbstractCovidData {

    @EmbeddedId
    private DataId dataId;

    /**
     * Instantiates a new City data.
     */
    public CityData() {
    }

    /**
     * Instantiates a new City data.
     *
     * @param dataId the data id
     */
    public CityData(DataId dataId) {
        this.dataId = dataId;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    @Transient
    public City getCity() {
        return dataId.getCity();
    }

    /**
     * Gets data id.
     *
     * @return the data id
     */
    public DataId getDataId() {
        return dataId;
    }

    /**
     * Sets data id.
     *
     * @param dataId the data id
     */
    public void setDataId(DataId dataId) {
        this.dataId = dataId;
    }


    /**
     * The type Data id.
     */
    @Embeddable
    public static class DataId implements Serializable {

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns(value = {@JoinColumn(name = "city", updatable = false), @JoinColumn(name = "state", updatable = false), @JoinColumn(name = "country", updatable = false)})
        private City city;

        /**
         * Instantiates a new Data id.
         */
        public DataId() {
        }

        /**
         * Instantiates a new Data id.
         *
         * @param city the city
         */
        public DataId(City city) {
            this.city = city;
        }

        /**
         * Gets city.
         *
         * @return the city
         */
        public City getCity() {
            return city;
        }

        /**
         * Sets city.
         *
         * @param city the city
         */
        public void setCity(City city) {
            this.city = city;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DataId dataId = (DataId) o;
            return Objects.equals(city, dataId.city);
        }

        @Override
        public int hashCode() {
            return Objects.hash(city);
        }
    }
}

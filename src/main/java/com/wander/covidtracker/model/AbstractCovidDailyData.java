package com.wander.covidtracker.model;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * The type Abstract covid daily data.
 */
@MappedSuperclass
public abstract class AbstractCovidDailyData {

    @Id
    @GeneratedValue
    private Long id;

    private LocalDate date;

    private long active;

    @AttributeOverrides({
            @AttributeOverride(name = "confirmed", column = @Column(name = "deltaConfirmed")),
            @AttributeOverride(name = "deceased", column = @Column(name = "deltaDeceased")),
            @AttributeOverride(name = "recovered", column = @Column(name = "deltaRecovered")),
            @AttributeOverride(name = "tested", column = @Column(name = "deltaTested"))
    })
    @Embedded
    private CovidData delta = new CovidData();


    @AttributeOverrides({
            @AttributeOverride(name = "confirmed", column = @Column(name = "totalConfirmed")),
            @AttributeOverride(name = "deceased", column = @Column(name = "totalDeceased")),
            @AttributeOverride(name = "recovered", column = @Column(name = "totalRecovered")),
            @AttributeOverride(name = "tested", column = @Column(name = "totalTested"))
    })
    @Embedded
    private CovidData total = new CovidData();

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    public long getActive() {
        return active;
    }

    /**
     * Sets active.
     *
     * @param activeCases the active cases
     */
    public void setActive(long activeCases) {
        this.active = activeCases;
    }

    /**
     * Gets delta.
     *
     * @return the delta
     */
    public CovidData getDelta() {
        return delta;
    }

    /**
     * Sets delta.
     *
     * @param delta the delta
     */
    public void setDelta(CovidData delta) {
        this.delta = delta;
    }

    /**
     * Gets total.
     *
     * @return the total
     */
    public CovidData getTotal() {
        return total;
    }

    /**
     * Sets total.
     *
     * @param total the total
     */
    public void setTotal(CovidData total) {
        this.total = total;
    }

}

package com.wander.covidtracker.model;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * The type Abstract covid data.
 */
@MappedSuperclass
public abstract class AbstractCovidData {

    private long active;

    private LocalDateTime lastUpdated;

    @AttributeOverrides({
            @AttributeOverride(name = "confirmed", column = @Column(name = "deltaConfirmed")),
            @AttributeOverride(name = "deceased", column = @Column(name = "deltaDeceased")),
            @AttributeOverride(name = "recovered", column = @Column(name = "deltaRecovered")),
            @AttributeOverride(name = "tested", column = @Column(name = "deltaTested"))
    })
    @Embedded
    private CovidData delta = new CovidData();


    @AttributeOverrides({
            @AttributeOverride(name = "confirmed", column = @Column(name = "totalConfirmed")),
            @AttributeOverride(name = "deceased", column = @Column(name = "totalDeceased")),
            @AttributeOverride(name = "recovered", column = @Column(name = "totalRecovered")),
            @AttributeOverride(name = "tested", column = @Column(name = "totalTested"))
    })
    @Embedded
    private CovidData total = new CovidData();

    /**
     * Gets active.
     *
     * @return the active
     */
    public long getActive() {
        return active;
    }

    /**
     * Sets active.
     *
     * @param activeCases the active cases
     */
    public void setActive(long activeCases) {
        this.active = activeCases;
    }

    /**
     * Gets delta.
     *
     * @return the delta
     */
    public CovidData getDelta() {
        return delta;
    }

    /**
     * Sets delta.
     *
     * @param delta the delta
     */
    public void setDelta(CovidData delta) {
        if (delta == null) {
            delta = new CovidData();
        }
        this.delta = delta;
    }

    /**
     * Gets total.
     *
     * @return the total
     */
    public CovidData getTotal() {
        return total;
    }

    /**
     * Sets total.
     *
     * @param total the total
     */
    public void setTotal(CovidData total) {
        this.total = total;
    }

    /**
     * Gets last updated.
     *
     * @return the last updated
     */
    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    /**
     * Sets last updated.
     *
     * @param lastUpdated the last updated
     */
    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * On pre persist.
     */
    @PrePersist
    void onPrePersist() {
        this.lastUpdated = LocalDateTime.now();
    }

    /**
     * On pre update.
     */
    @PreUpdate
    void onPreUpdate() {
        this.lastUpdated = LocalDateTime.now();
    }
}

package com.wander.covidtracker.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * The type State.
 */
@Entity
@Table(name = "state")
public class State {

    @EmbeddedId
    private StateId stateId;

    private String name;

    @OneToMany
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    @JoinColumns(value = {@JoinColumn(name = "state"), @JoinColumn(name = "country")})
    private List<City> cities;

    /**
     * Instantiates a new State.
     */
    public State() {
    }

    /**
     * Instantiates a new State.
     *
     * @param stateId the state id
     */
    public State(StateId stateId){
        this.stateId = stateId;
    }

    /**
     * Instantiates a new State.
     *
     * @param code    the code
     * @param country the country
     */
    public State(String code, Country country){
        this.stateId = new StateId(code, country);
    }

    /**
     * Gets state id.
     *
     * @return the state id
     */
    public StateId getStateId() {
        return stateId;
    }

    /**
     * Sets state id.
     *
     * @param stateId the state id
     */
    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return stateId.code;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public Country getCountry() {
        return stateId.country;
    }

    /**
     * Gets cities.
     *
     * @return the cities
     */
    public List<City> getCities() {
        return cities;
    }

    /**
     * Sets cities.
     *
     * @param cities the cities
     */
    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    /**
     * The type State id.
     */
    @Embeddable
    public static class StateId implements Serializable {

        @Column(length = 5)
        private String code;

        @ManyToOne
        @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
        @JoinColumn(name = "country")
        private Country country;

        /**
         * Instantiates a new State id.
         */
        public StateId() {
        }

        /**
         * Instantiates a new State id.
         *
         * @param code    the code
         * @param country the country
         */
        public StateId(String code, Country country) {
            this.code = code;
            this.country = country;
        }

        /**
         * Gets code.
         *
         * @return the code
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets code.
         *
         * @param code the code
         */
        public void setCode(String code) {
            this.code = code;
        }

        /**
         * Gets country.
         *
         * @return the country
         */
        public Country getCountry() {
            return country;
        }

        /**
         * Sets country.
         *
         * @param country the country
         */
        public void setCountry(Country country) {
            this.country = country;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            StateId stateId = (StateId) o;
            return Objects.equals(code, stateId.code) &&
                    Objects.equals(country, stateId.country);
        }

        @Override
        public int hashCode() {
            return Objects.hash(code, country);
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", StateId.class.getSimpleName() + "[", "]")
                    .add("code='" + code + "'")
                    .add("country=" + country)
                    .toString();
        }
    }
}

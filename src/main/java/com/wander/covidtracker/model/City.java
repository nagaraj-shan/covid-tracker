package com.wander.covidtracker.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * The type City.
 */
@Entity
@Table(name = "city")
public class City {

    @EmbeddedId
    private CityId cityId;

    /**
     * Instantiates a new City.
     */
    public City() {

    }

    /**
     * Instantiates a new City.
     *
     * @param cityId the city id
     */
    public City(CityId cityId) {
        this.cityId = cityId;
    }

    /**
     * Instantiates a new City.
     *
     * @param name  the name
     * @param state the state
     */
    public City(String name, State state) {
        this.cityId = new CityId(name, state);
    }

    /**
     * Gets city id.
     *
     * @return the city id
     */
    public CityId getCityId() {
        return cityId;
    }

    /**
     * Sets city id.
     *
     * @param cityId the city id
     */
    public void setCityId(CityId cityId) {
        this.cityId = cityId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    @Transient
    public String getName() {
        return cityId.name;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    @Transient
    public State getState() {
        return cityId.state;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    @Transient
    public Country getCountry() {
        return getState().getCountry();
    }


    /**
     * The type City id.
     */
    @Embeddable
    public static class CityId implements Serializable {

        private String name;

        @ManyToOne
        @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
        @JoinColumns(value = {@JoinColumn(name = "state"), @JoinColumn(name = "country")})
        private State state;

        /**
         * Instantiates a new City id.
         */
        public CityId() {
        }

        /**
         * Instantiates a new City id.
         *
         * @param name  the name
         * @param state the state
         */
        public CityId(String name, State state) {
            this.name = name;
            this.state = state;
        }

        /**
         * Gets name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Sets name.
         *
         * @param name the name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Gets state.
         *
         * @return the state
         */
        public State getState() {
            return state;
        }

        /**
         * Sets state.
         *
         * @param state the state
         */
        public void setState(State state) {
            this.state = state;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CityId cityId = (CityId) o;
            return Objects.equals(name, cityId.name) &&
                    Objects.equals(state, cityId.state);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, state);
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", CityId.class.getSimpleName() + "[", "]")
                    .add("name='" + name + "'")
                    .add("state=" + state)
                    .toString();
        }
    }
}

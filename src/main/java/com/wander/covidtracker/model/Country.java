package com.wander.covidtracker.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

/**
 * The type Country.
 */
@Entity
@Table(name = "country")
public class Country {

    @Id
    @Column(length = 5)
    private String code;

    @Column(length = 50)
    private String name;

    @OneToMany(fetch = FetchType.LAZY)
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "country")
    private List<State> states;

    /**
     * Instantiates a new Country.
     */
    public Country(){

    }

    /**
     * Instantiates a new Country.
     *
     * @param code the code
     * @param name the name
     */
    public Country(String code, String name){
        this.code = code;
        this.name = name;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets states.
     *
     * @return the states
     */
    public List<State> getStates() {
        return states;
    }

    /**
     * Sets states.
     *
     * @param states the states
     */
    public void setStates(List<State> states) {
        this.states = states;
    }
}

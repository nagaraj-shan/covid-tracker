package com.wander.covidtracker.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * The type State data.
 */
@Entity
@Table(name = "state_data")
public class StateData extends AbstractCovidData {

    @EmbeddedId
    private DataId dataId;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns(value = {@JoinColumn(name = "state", updatable = false), @JoinColumn(name = "country" , updatable = false)})
    private List<CityData> cityData;

    /**
     * Instantiates a new State data.
     */
    public StateData() {
    }

    /**
     * Instantiates a new State data.
     *
     * @param dataId the data id
     */
    public StateData(DataId dataId) {
        this.dataId = dataId;
    }

    /**
     * Gets data id.
     *
     * @return the data id
     */
    public DataId getDataId() {
        return dataId;
    }

    /**
     * Sets data id.
     *
     * @param dataId the data id
     */
    public void setDataId(DataId dataId) {
        this.dataId = dataId;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    @Transient
    public State getState() {
        return dataId.getState();
    }

    /**
     * Gets city data.
     *
     * @return the city data
     */
    public List<CityData> getCityData() {
        return cityData;
    }

    /**
     * Sets city data.
     *
     * @param cityData the city data
     */
    public void setCityData(List<CityData> cityData) {
        this.cityData = cityData;
    }


    /**
     * The type Data id.
     */
    @Embeddable
    public static class DataId implements Serializable {

        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumns(value = {@JoinColumn(name = "state"), @JoinColumn(name = "country")})
        private State state;

        /**
         * Instantiates a new Data id.
         */
        public DataId() {

        }

        /**
         * Instantiates a new Data id.
         *
         * @param state the state
         */
        public DataId(State state) {
            this.state = state;
        }

        /**
         * Gets state.
         *
         * @return the state
         */
        public State getState() {
            return state;
        }

        /**
         * Sets state.
         *
         * @param state the state
         */
        public void setState(State state) {
            this.state = state;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DataId dataId = (DataId) o;
            return Objects.equals(state, dataId.state);
        }

        @Override
        public int hashCode() {
            return Objects.hash(state);
        }
    }
}

package com.wander.covidtracker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * The type State daily data.
 */
@Entity
@Table(name = "state_daily_data")
public class StateDailyData extends AbstractCovidDailyData {

    @Column
    private String country;

    @Column
    private String state;

    @Transient
    private List<CityDailyData> cityDailyData;

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets state.
     *
     * @param state the state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Sets city daily data.
     *
     * @param cityDailyData the city daily data
     */
    public void setCityDailyData(List<CityDailyData> cityDailyData) {
        this.cityDailyData = cityDailyData;
    }

    /**
     * Gets city daily data.
     *
     * @return the city daily data
     */
    public List<CityDailyData> getCityDailyData() {
        return cityDailyData;
    }
}

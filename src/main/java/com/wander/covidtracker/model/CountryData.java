package com.wander.covidtracker.model;

import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * The type Country data.
 */
@Entity
@Table(name = "country_data")
public class CountryData extends AbstractCovidData {

    @EmbeddedId
    private DataId dataId;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumns(value = {@JoinColumn(name = "country")})
    private List<StateData> stateData;

    /**
     * Instantiates a new Country data.
     */
    public CountryData() {

    }

    /**
     * Instantiates a new Country data.
     *
     * @param dataId the data id
     */
    public CountryData(DataId dataId) {
        this.dataId = dataId;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    @Transient
    public Country getCountry() {
        return dataId.getCountry();
    }

    /**
     * Gets data id.
     *
     * @return the data id
     */
    public DataId getDataId() {
        return dataId;
    }

    /**
     * Sets data id.
     *
     * @param dataId the data id
     */
    public void setDataId(DataId dataId) {
        this.dataId = dataId;
    }

    /**
     * Gets state data.
     *
     * @return the state data
     */
    public List<StateData> getStateData() {
        return stateData;
    }

    /**
     * Sets state data.
     *
     * @param stateData the state data
     */
    public void setStateData(List<StateData> stateData) {
        this.stateData = stateData;
    }

    /**
     * Get state data state data.
     *
     * @param state the state
     * @return the state data
     */
    public StateData getStateData(State state){
        if(CollectionUtils.isEmpty(stateData)) {
            return null;
        }
        return stateData.stream().filter(stateData -> stateData.getState().equals(state)).findAny().orElse(null);
    }

    /**
     * The type Data id.
     */
    @Embeddable
    public static class DataId implements Serializable {

        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumns(value = {@JoinColumn(name = "country")})
        private Country country;

        /**
         * Instantiates a new Data id.
         */
        public DataId() {
        }

        /**
         * Instantiates a new Data id.
         *
         * @param country the country
         */
        public DataId(Country country) {
            this.country = country;
        }

        /**
         * Gets country.
         *
         * @return the country
         */
        public Country getCountry() {
            return country;
        }

        /**
         * Sets country.
         *
         * @param country the country
         */
        public void setCountry(Country country) {
            this.country = country;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DataId dataId = (DataId) o;
            return Objects.equals(country, dataId.country);
        }

        @Override
        public int hashCode() {
            return Objects.hash(country);
        }
    }
}

package com.wander.covidtracker.web;

import com.wander.covidtracker.model.User;
import com.wander.covidtracker.service.UserService;
import com.wander.covidtracker.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * The type User controller.
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    /**
     * User form user.
     *
     * @return the user
     */
    @ModelAttribute("userForm")
    public User userForm() {
        return new User();

    }

    /**
     * Register string.
     *
     * @param userForm      the user form
     * @param bindingResult the binding result
     * @return the string
     */
    @PostMapping("/register")
    public String register(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            bindingResult.getModel().put("register", true);
            return "login";
        }

        userService.save(userForm);

        return "redirect:/index";
    }

    /**
     * Login string.
     *
     * @param request the request
     * @param model   the model
     * @param error   the error
     * @param logout  the logout
     * @return the string
     */
    @GetMapping({"/login", "/register"})
    public String login(HttpServletRequest request, Model model, String error, String logout) {
        if(request.getRequestURI().contains("/register")){
            model.addAttribute("register", true);
        }
        if (error != null) {
            model.addAttribute("loginError", true);
        }

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        model.addAttribute("userForm", new User());

        return "login";
    }

}

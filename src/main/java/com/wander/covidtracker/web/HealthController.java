package com.wander.covidtracker.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * The type Health controller.
 */
@Controller
public class HealthController {

    /**
     * Health string.
     *
     * @return the string
     * @throws IOException the io exception
     */
    @GetMapping("/health")
    @ResponseBody
    public String health() throws IOException {
        return "Ok";
    }
}

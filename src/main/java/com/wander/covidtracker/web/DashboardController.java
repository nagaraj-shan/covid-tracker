package com.wander.covidtracker.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wander.covidtracker.model.CountryDailyData;
import com.wander.covidtracker.model.CountryData;
import com.wander.covidtracker.service.CovidDataService;
import com.wander.covidtracker.service.DataSyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * The type Dashboard controller.
 */
@Controller
public class DashboardController {

    /**
     * The constant COUNTRY_CODE.
     */
    public static final String COUNTRY_CODE = "IN";

    @Autowired
    private CovidDataService dataService;

    @Autowired
    private DataSyncService syncService;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Dashboard string.
     *
     * @param model the model
     * @return the string
     * @throws IOException the io exception
     */
    @GetMapping({"/", "/index"})
    public String dashboard(Model model) throws IOException {
        CountryData countryData = dataService.getCountryData(COUNTRY_CODE);
        model.addAttribute("countryData", countryData);
        double meanValue = dataService.getMeanValue(COUNTRY_CODE, 10);
        double growthRate = (meanValue - 1) * 100;
        long confirmed = countryData.getTotal().getConfirmed();
        long expectedIn24hours = (long) (confirmed * (meanValue - 1));
        long expectedIn5day = (long) (confirmed * (Math.pow(meanValue, 5)) - confirmed);
        model.addAttribute("growthRate", growthRate);
        model.addAttribute("expectedIn24hours", expectedIn24hours);
        model.addAttribute("expectedIn5day", expectedIn5day);
        List<CountryDailyData> countryDataLastNdays = dataService.getCountryDataLastNdays(COUNTRY_CODE, 30);
        Collections.reverse(countryDataLastNdays);
        String json = objectMapper.writeValueAsString(countryDataLastNdays);
        model.addAttribute("last30Days", json);
        return "index";
    }

    /**
     * Cities map.
     *
     * @return the map
     * @throws IOException the io exception
     */
    @GetMapping({"/statesAndCities"})
    @ResponseBody
    public Map<String, String> cities() throws IOException {
        return dataService.getAllStatesAndCities();
    }
}

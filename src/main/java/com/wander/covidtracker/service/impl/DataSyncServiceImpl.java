package com.wander.covidtracker.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wander.covidtracker.dto.CovidApiData;
import com.wander.covidtracker.dto.CovidApiStateData;
import com.wander.covidtracker.model.*;
import com.wander.covidtracker.repository.*;
import com.wander.covidtracker.service.DataSyncService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Data sync service.
 */
@Service
@Profile("!test")
public class DataSyncServiceImpl implements DataSyncService {

    private static final String DAILY_DATA_URL = "https://api.covid19india.org/v4/data.json";
    private static final String ALL_DATA_URL = "https://api.covid19india.org/v4/data-all.json";
    private static final String SPECIFIC_DATE_DATA_URL = "https://api.covid19india.org/v4/data-%d-%02d-%02d.json";
    private static final String DISTRICT_DATA_URL = "https://api.covid19india.org/state_district_wise.json";
    /**
     * The constant DEFAULT_START_DATE.
     */
    public static final LocalDate DEFAULT_START_DATE = LocalDate.of(2020, 1, 1);

    private static final Logger logger = LoggerFactory.getLogger(DataSyncServiceImpl.class);
    /**
     * The constant TOTAL_KEY.
     */
    public static final String TOTAL_KEY = "TT";

    private final CloseableHttpClient httpClient = getHttpClient();

    private CloseableHttpClient getHttpClient() {
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
        connManager.setMaxTotal(100);
        connManager.setDefaultMaxPerRoute(10);
        return HttpClients.custom().setConnectionManager(connManager).build();
    }

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CountryRepository countryRepo;

    @Autowired
    private StateRepository stateRepo;

    @Autowired
    private CityRepository cityRepo;

    @Autowired
    private StateDailyDataRepository stateDailyDataRepo;

    @Autowired
    private CountryDailyDataRepository countryDailyDataRepo;

    @Autowired
    private CityDailyDataRepository cityDailyDataRepo;

    @Autowired
    private CountryDataRepository countryDataRepo;

    @Autowired
    private CityDataRepository cityDataRepo;

    @Autowired
    private StateDataRepository stateDataRepo;

    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    @Scheduled(cron = "0 */30 * * * *")
    @Override
    @CacheEvict(cacheNames="countryData", allEntries=true)
    public void syncLatestData() {
        List<Country> countries = countryRepo.findAll();
        countries.forEach(country -> {
            try {
                logger.info("Sync latest Data");
                syncLatestData(country);
                logger.info("Synced latest Data");
            } catch (Exception e) {
                logger.error("Error while sync data for country {}", country, e);
            }
        });
    }

    @Transactional
    @Scheduled(cron = "0 0 5 * * ?")
    @EventListener(ApplicationReadyEvent.class)
    @CacheEvict(cacheNames="countryDailyData", allEntries=true)
    public void syncDailyData() {
        List<Country> countries = countryRepo.findAll();
        countries.forEach(country -> {
            try {
                logger.info("Sync Daily Data");
                syncDailyData(country);
                logger.info("Synced Daily Data");
            } catch (Exception e) {
                logger.error("Error while sync data for country {}", country, e);
            }
        });
    }

    @Override
    public void syncDailyData(final Country country) {
        LocalDate lastDataDate = countryDailyDataRepo.lastDataDate(country.getCode()).orElse(DEFAULT_START_DATE);
        LocalDate now = LocalDate.now();
        List<CountryDailyData> countryDailyData = Stream.iterate(lastDataDate, date -> date.plusDays(1))
                                                        .limit(ChronoUnit.DAYS.between(lastDataDate, now))
                                                        .map(date -> getCountryDailyData(country, date))
                                                        .collect(Collectors.toList());
        countryDailyData.stream().filter(Objects::nonNull).sorted(Comparator.comparing(AbstractCovidDailyData::getDate))
                        .forEach(this::saveCountryDailyData);
    }

    private CountryDailyData getCountryDailyData(Country country, LocalDate date) {
        String dateDataUrl = String
                .format(SPECIFIC_DATE_DATA_URL, date.getYear(), date.getMonthValue(), date
                        .getDayOfMonth());
        try {
            byte[] jsonData = downloadUrl(dateDataUrl);
            if (jsonData != null) {
                Map<String, CovidApiStateData> o = objectMapper
                        .readValue(jsonData, new TypeReference<Map<String, CovidApiStateData>>() {
                        });
                List<StateDailyData> stateDailyDataList = o.entrySet().stream().map(entry -> {
                    CovidApiStateData value = entry.getValue();
                    State state = stateRepo.getOne(new State.StateId(entry.getKey(), country));
                    logger.info("Loading state daily data {}:{}", state.getCode(), state.getName());
                    return getStateDailyData(state, date, value);
                }).collect(Collectors.toList());
                CountryDailyData countryDailyData = getCountryDailyData(country, o, date, stateDailyDataList);
                countryDailyData.setStateDailyData(stateDailyDataList);
                return countryDailyData;
            }
        } catch (Exception e) {
            logger.error("Error while fetching data ", e);
        }
        return null;
    }

    private void saveCountryDailyData(CountryDailyData countryDailyData) {
        countryDailyDataRepo.save(countryDailyData);
        stateDailyDataRepo.saveAll(countryDailyData.getStateDailyData());
        List<CityDailyData> cityDailyData = countryDailyData.getStateDailyData().stream()
                                                            .filter(stateDailyData -> !CollectionUtils
                                                                    .isEmpty(stateDailyData.getCityDailyData()))
                                                            .flatMap((Function<StateDailyData, Stream<CityDailyData>>) stateDailyData -> stateDailyData
                                                                    .getCityDailyData().stream())
                                                            .collect(Collectors.toList());
        cityDailyDataRepo.saveAll(cityDailyData);
    }

    private CountryDailyData getCountryDailyData(Country country, Map<String, CovidApiStateData> o, LocalDate dataDate, List<StateDailyData> stateDailyDataList) {
        CovidApiStateData allStates = o.get(TOTAL_KEY);
        CountryDailyData countryDailyData;
        if (allStates != null) {
            countryDailyData = countryDailyDataRepo.findByDateAndCountry(dataDate, country.getCode())
                                                   .orElse(new CountryDailyData());
            countryDailyData.setDate(dataDate);
            countryDailyData.setCountry(country.getCode());
            countryDailyData.setTotal(allStates.getTotal());
            countryDailyData.setDelta(allStates.getDelta());
            countryDailyData.setActive(allStates.getTotal().getActive());
            countryDailyData.setStateDailyData(stateDailyDataList);
        } else {
            countryDailyData = getCountryDailyData(dataDate, country, stateDailyDataList);
        }
        return countryDailyData;
    }

    private CountryDailyData getCountryDailyData(LocalDate dataDate, Country country, List<StateDailyData> stateDailyDataList) {
        StateDailyData stateDailyData1 = stateDailyDataList.stream().reduce((stateDailyData, stateDailyData2) -> {
            stateDailyData.setTotal(addCovidData(stateDailyData.getTotal(), stateDailyData.getTotal()));
            stateDailyData.setDelta(addCovidData(stateDailyData.getDelta(), stateDailyData.getDelta()));
            return stateDailyData;
        }).orElseGet(StateDailyData::new);
        CountryDailyData countryDailyData = new CountryDailyData();
        countryDailyData.setDate(dataDate);
        countryDailyData.setCountry(country.getCode());
        countryDailyData.setTotal(stateDailyData1.getTotal());
        countryDailyData.setDelta(stateDailyData1.getDelta());
        countryDailyData.setActive(stateDailyData1.getTotal().getActive());
        return countryDailyData;
    }

    private StateDailyData getStateDailyData(State state, LocalDate dataDate, CovidApiStateData apiStateData) {
        try {
            StateDailyData stateDailyData = stateDailyDataRepo
                    .findByDateAndCountryAndState(dataDate, state.getCountry().getCode(), state.getCode())
                    .orElse(new StateDailyData());
            ;
            stateDailyData.setDate(dataDate);
            stateDailyData.setCountry(state.getCountry().getCode());
            stateDailyData.setState(state.getCode());
            stateDailyData.setTotal(apiStateData.getTotal());
            stateDailyData.setDelta(apiStateData.getDelta());
            stateDailyData.setActive(apiStateData.getActive());
            if (apiStateData.getDistricts() != null) {
                List<CityDailyData> cityDailyData = getCityDailyData(state, apiStateData.getDistricts(), dataDate);
                stateDailyData.setCityDailyData(cityDailyData);
            }
            return stateDailyData;
        } catch (Exception e) {
            logger.error("Exception : {}", apiStateData, e);
            throw e;
        }

    }

    private List<CityDailyData> getCityDailyData(State state, Map<String, CovidApiData> cities, LocalDate dataDate) {
        return cities.entrySet().stream().map(entry -> {
            City city = cityRepo.findById(new City.CityId(entry.getKey(), state)).orElseGet(() -> {
                City c = new City(entry.getKey(), state);
                cityRepo.saveAndFlush(c);
                return c;
            });
            CityDailyData data = cityDailyDataRepo
                    .findByDateAndCountryAndStateAndCity(dataDate, city.getCountry().getCode(),
                            city.getState().getCode(),
                            city.getName()).orElse(new CityDailyData());
            data.setDate(dataDate);
            data.setCity(city.getName());
            data.setCountry(state.getCountry().getCode());
            data.setState(state.getCode());
            CovidApiData value = entry.getValue();
            data.setTotal(value.getTotal());
            data.setDelta(value.getDelta());
            data.setActive(entry.getValue().getActive());
            return data;
        }).collect(Collectors.toList());
    }

    private void syncLatestData(Country country) {
        try {
            byte[] jsonData = downloadUrl(DAILY_DATA_URL);
            if (jsonData != null) {
                Map<String, CovidApiStateData> o = objectMapper
                        .readValue(jsonData, new TypeReference<Map<String, CovidApiStateData>>() {
                        });
                List<StateData> stateDataList = o.entrySet().stream().map(entry -> {
                    CovidApiStateData value = entry.getValue();
                    State state = stateRepo.findById(new State.StateId(entry.getKey(), country)).get();
                    return getStateData(state, value);
                }).collect(Collectors.toList());
                CountryData countryData = getCountryData(country, stateDataList, o);
                if (countryData != null) {
                    stateDataList.stream().filter(stateData -> !CollectionUtils.isEmpty(stateData.getCityData()))
                                 .forEach(stateData -> cityDataRepo.saveAll(stateData.getCityData()));
                    cityDataRepo.flush();
                    stateDataRepo.saveAll(stateDataList);
                    stateDataRepo.flush();
                    countryData.setStateData(stateDataList);
                    countryDataRepo.save(countryData);
                }
            }
        } catch (Exception e) {
            logger.error("Error while fetching data ", e);
        }
    }

    private CountryData getCountryData(Country country, List<StateData> stateDataList, Map<String, CovidApiStateData> o) {
        CovidApiStateData allStates = o.get(TOTAL_KEY);
        if (allStates != null) {
            CountryData data = new CountryData();
            data.setDataId(new CountryData.DataId(country));
            data.setTotal(allStates.getTotal());
            data.setDelta(allStates.getDelta());
            data.setActive(allStates.getTotal().getActive());
            data.setStateData(stateDataList);
            return data;
        }
        return null;
    }

    private StateData getStateData(State state, CovidApiStateData apiStateData) {
        try {
            StateData.DataId id = new StateData.DataId(state);
            StateData data = stateDataRepo.findById(id).orElse(new StateData(id));
            data.setTotal(apiStateData.getTotal());
            data.setDelta(apiStateData.getDelta());
            data.setActive(apiStateData.getActive());
            if (apiStateData.getDistricts() != null) {
                List<CityData> cityData = getCityData(state, apiStateData.getDistricts());
                data.setCityData(cityData);
            }
            return data;
        } catch (Exception e) {
            logger.error("Exception : {}", apiStateData, e);
            throw e;
        }
    }

    private List<CityData> getCityData(State state, Map<String, CovidApiData> cities) {
        return cities.entrySet().stream().map(entry -> {
            City city = cityRepo.findById(new City.CityId(entry.getKey(), state)).orElseGet(() -> {
                City c = new City(entry.getKey(), state);
                cityRepo.saveAndFlush(c);
                return c;
            });
            CityData.DataId id = new CityData.DataId(city);
            CityData data = cityDataRepo.findById(id).orElse(new CityData(id));
            CovidApiData value = entry.getValue();
            data.setTotal(value.getTotal());
            data.setDelta(value.getDelta());
            data.setActive(entry.getValue().getActive());
            return data;
        }).collect(Collectors.toList());
    }

    private CovidData addCovidData(CovidData data1, CovidData data2) {
        if (data1 == null) {
            data1 = new CovidData();
        }
        if (data2 == null) {
            data2 = new CovidData();
        }
        CovidData data = new CovidData();
        data.setConfirmed(data1.getConfirmed() + data2.getConfirmed());
        data.setRecovered(data1.getRecovered() + data2.getRecovered());
        data.setDeceased(data1.getDeceased() + data2.getDeceased());
        data.setTested(data1.getTested() + data2.getTested());
        return data;
    }

    private JsonNode getJsonNode(String url) throws IOException {
        byte[] data = downloadUrl(url);
        if (data != null) {
            return objectMapper.readTree(data);
        }
        return null;
    }

    private byte[] downloadUrl(String url) throws IOException {
        logger.info("Loading data from {}", url);
        HttpGet request = new HttpGet(url);
        try (CloseableHttpResponse response = httpClient.execute(request)) {
            if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() < 300) {
                return EntityUtils.toByteArray(response.getEntity());
            }
        }
        return null;
    }
}

package com.wander.covidtracker.service.impl;

import com.wander.covidtracker.exception.ResourceNotFoundException;
import com.wander.covidtracker.model.*;
import com.wander.covidtracker.repository.*;
import com.wander.covidtracker.service.CovidDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * The type Covid data service.
 */
@Service
public class CovidDataServiceImpl implements CovidDataService {

    @Autowired
    private CountryDataRepository countryDataRepo;

    @Autowired
    private StateDataRepository stateDataRepo;

    @Autowired
    private CityDataRepository cityDataRepo;

    @Autowired
    private CountryDailyDataRepository countryDailyDataRepo;

    @Autowired
    private CityDailyDataRepository cityDailyDataRepo;

    @Autowired
    private StateDailyDataRepository stateDailyDataRepo;

    @Autowired
    private CountryRepository countryRepo;

    @Autowired
    private StateRepository stateRepo;

    @Autowired
    private CityRepository cityRepo;

    @Override
    @Cacheable("countryData")
    public CountryData getCountryData(String countryCode) {
        return countryDataRepo.findById(new CountryData.DataId(getCountry(countryCode)))
                              .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public List<StateData> getStateData(String countryCode) {
        return getCountryData(countryCode).getStateData();
    }

    @Override
    public StateData getStateData(String countryCode, String stateCode) {
        State state = getState(countryCode, stateCode);
        return stateDataRepo.findById(new StateData.DataId(state))
                            .orElseThrow((Supplier<RuntimeException>) ResourceNotFoundException::new);
    }

    @Override
    public List<CityData> getCityData(String countryCode, String stateCode) {
        return getStateData(countryCode, stateCode).getCityData();
    }

    @Override
    public List<CountryDailyData> getStateDailyData(String countryCode) {
        return countryDailyDataRepo.findAllByCountry(countryCode);
    }

    @Override
    public List<StateDailyData> getStateDailyData(String countryCode, String stateCode) {
        return stateDailyDataRepo.findAllByCountryAndState(countryCode, stateCode);
    }

    @Override
    public List<CityDailyData> getCityDailyData(String countryCode, String stateCode, String cityName) {
        return cityDailyDataRepo.findAllByCountryAndStateAndCity(countryCode, stateCode, cityName);
    }

    @Override
    public CityData getCityData(String countryCode, String stateCode, String cityName) {
        City city = getCity(countryCode, stateCode, cityName);
        return cityDataRepo.findById(new CityData.DataId(city))
                           .orElseThrow((Supplier<RuntimeException>) ResourceNotFoundException::new);
    }

    @Override
    public double getMeanValue(String countryCode, int numberOfDays) {
        if(numberOfDays <= 0){
            return 0.0;
        }
        List<Long> confirmedForNdays = countryDailyDataRepo.getConfirmedForNdays(countryCode, PageRequest
                .of(0, numberOfDays + 1, Sort.by(Sort.Direction.DESC, "date")));
        if(CollectionUtils.isEmpty(confirmedForNdays)){
            return 0.0;
        }
        Collections.reverse(confirmedForNdays);
        double mean = 0;
        for (int i = 1; i < confirmedForNdays.size(); i++) {
            mean += confirmedForNdays.get(i).doubleValue() / confirmedForNdays.get(i - 1).doubleValue();
        }
        return mean / numberOfDays;
    }

    @Override
    public Map<String, String> getAllStatesAndCities() {
        List<City> all = cityRepo.findAll();
        Map<String, String> cities = new HashMap<>();
        all.forEach(city -> {
            cities.put(city.getName(), city.getName());
            cities.put(city.getState().getCode(), city.getState().getName());
        });
        return cities;
    }

    @Override
    @Cacheable("countryDailyData")
    public List<CountryDailyData> getCountryDataLastNdays(String countryCode, int numberOfDays) {
        PageRequest pageRequest = PageRequest
                .of(0, numberOfDays + 1, Sort.by(Sort.Direction.DESC, "date"));
        return countryDailyDataRepo.findByCountry(countryCode, pageRequest);
    }

    /**
     * Gets country.
     *
     * @param countryCode the country code
     * @return the country
     */
    @Cacheable("country")
    public Country getCountry(String countryCode) {
        return countryRepo.findById(countryCode)
                          .orElseThrow((Supplier<RuntimeException>) () -> new ResourceNotFoundException("Invalid Country Code " + countryCode));
    }

    /**
     * Gets state.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @return the state
     */
    @Cacheable("state")
    public State getState(String countryCode, String stateCode) {
        Country country = getCountry(countryCode);
        return stateRepo.findById(new State.StateId(stateCode, country))
                        .orElseThrow((Supplier<RuntimeException>) () -> new ResourceNotFoundException("Invalid State Code " + stateCode));
    }

    /**
     * Gets city.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @param cityName    the city name
     * @return the city
     */
    @Cacheable("city")
    public City getCity(String countryCode, String stateCode, String cityName) {
        State state = getState(countryCode, stateCode);
        return cityRepo.findById(new City.CityId(cityName, state))
                       .orElseThrow((Supplier<RuntimeException>) () -> new ResourceNotFoundException("Invalid city name " + cityName));
    }
}

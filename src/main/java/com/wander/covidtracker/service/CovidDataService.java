package com.wander.covidtracker.service;

import com.wander.covidtracker.model.*;

import java.util.List;
import java.util.Map;

/**
 * The interface Covid data service.
 */
public interface CovidDataService {
    /**
     * Gets country data.
     *
     * @param countryCode the country code
     * @return the country data
     */
    CountryData getCountryData(String countryCode);

    /**
     * Gets state data.
     *
     * @param countryCode the country code
     * @return the state data
     */
    List<StateData> getStateData(String countryCode);

    /**
     * Gets state data.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @return the state data
     */
    StateData getStateData(String countryCode, String stateCode);

    /**
     * Gets city data.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @param cityName    the city name
     * @return the city data
     */
    CityData getCityData(String countryCode, String stateCode, String cityName);

    /**
     * Gets city data.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @return the city data
     */
    List<CityData> getCityData(String countryCode, String stateCode);

    /**
     * Gets state daily data.
     *
     * @param countryCode the country code
     * @return the state daily data
     */
    List<CountryDailyData> getStateDailyData(String countryCode);

    /**
     * Gets state daily data.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @return the state daily data
     */
    List<StateDailyData> getStateDailyData(String countryCode, String stateCode);

    /**
     * Gets city daily data.
     *
     * @param countryCode the country code
     * @param stateCode   the state code
     * @param cityName    the city name
     * @return the city daily data
     */
    List<CityDailyData> getCityDailyData(String countryCode, String stateCode, String cityName);

    /**
     * Gets country data last ndays.
     *
     * @param countryCode  the country code
     * @param numberOfDays the number of days
     * @return the country data last ndays
     */
    List<CountryDailyData> getCountryDataLastNdays(String countryCode, int numberOfDays);

    /**
     * Gets mean value.
     *
     * @param countryCode  the country code
     * @param numberOfDays the number of days
     * @return the mean value
     */
    double getMeanValue(String countryCode, int numberOfDays);

    /**
     * Gets all states and cities.
     *
     * @return the all states and cities
     */
    Map<String, String> getAllStatesAndCities();
}

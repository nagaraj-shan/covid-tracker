package com.wander.covidtracker.service;

import com.wander.covidtracker.model.Country;

import java.io.IOException;

/**
 * The interface Data sync service.
 */
public interface DataSyncService {
    /**
     * Sync daily data.
     */
    void syncDailyData();

    /**
     * Sync latest data.
     */
    void syncLatestData();

    /**
     * Sync daily data.
     *
     * @param country the country
     * @throws IOException the io exception
     */
    void syncDailyData(Country country) throws IOException;
}

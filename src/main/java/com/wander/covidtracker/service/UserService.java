package com.wander.covidtracker.service;

import com.wander.covidtracker.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * The interface User service.
 */
public interface UserService extends UserDetailsService {

    /**
     * Save.
     *
     * @param user the user
     */
    void save(User user);

    /**
     * Find by username user.
     *
     * @param username the username
     * @return the user
     */
    User findByUsername(String username);

    /**
     * Find logged in username string.
     *
     * @return the string
     */
    String findLoggedInUsername();
}

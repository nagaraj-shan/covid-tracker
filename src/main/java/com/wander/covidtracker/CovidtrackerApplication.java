package com.wander.covidtracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The type Covidtracker application.
 */
@SpringBootApplication
@EnableScheduling
@EnableCaching
public class CovidtrackerApplication {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
		SpringApplication.run(CovidtrackerApplication.class, args);
	}

}

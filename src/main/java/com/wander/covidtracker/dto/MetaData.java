package com.wander.covidtracker.dto;

import java.util.StringJoiner;

/**
 * The type Meta data.
 */
public class MetaData {

    private long population;

    /**
     * Gets population.
     *
     * @return the population
     */
    public long getPopulation() {
        return population;
    }

    /**
     * Sets population.
     *
     * @param population the population
     */
    public void setPopulation(long population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MetaData.class.getSimpleName() + "[", "]")
                .add("population=" + population)
                .toString();
    }
}

package com.wander.covidtracker.dto;

import com.wander.covidtracker.model.CovidData;

import java.util.StringJoiner;

/**
 * The type Covid api data.
 */
public class CovidApiData {

    /**
     * The Delta.
     */
    protected CovidData delta;

    /**
     * The Total.
     */
    protected CovidData total;

    /**
     * The Meta data.
     */
    protected MetaData metaData;

    /**
     * Gets delta.
     *
     * @return the delta
     */
    public CovidData getDelta() {
        return delta;
    }

    /**
     * Sets delta.
     *
     * @param delta the delta
     */
    public void setDelta(CovidData delta) {
        this.delta = delta;
    }

    /**
     * Gets total.
     *
     * @return the total
     */
    public CovidData getTotal() {
        return total;
    }

    /**
     * Sets total.
     *
     * @param total the total
     */
    public void setTotal(CovidData total) {
        this.total = total;
    }

    /**
     * Gets meta data.
     *
     * @return the meta data
     */
    public MetaData getMetaData() {
        return metaData;
    }

    /**
     * Sets meta data.
     *
     * @param metaData the meta data
     */
    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CovidApiData.class.getSimpleName() + "[", "]")
                .add("delta=" + delta)
                .add("total=" + total)
                .add("metaData=" + metaData)
                .toString();
    }

    /**
     * Gets active.
     *
     * @return the active
     */
    public long getActive() {
        if (total == null) {
            return 0;
        }
        return total.getConfirmed() - (total.getRecovered() + total.getDeceased());
    }
}

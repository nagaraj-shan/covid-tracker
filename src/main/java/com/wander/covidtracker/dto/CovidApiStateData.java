package com.wander.covidtracker.dto;

import java.util.Map;
import java.util.StringJoiner;

/**
 * The type Covid api state data.
 */
public class CovidApiStateData extends CovidApiData {

    private Map<String, CovidApiData> districts;

    /**
     * Gets districts.
     *
     * @return the districts
     */
    public Map<String, CovidApiData> getDistricts() {
        return districts;
    }

    /**
     * Sets districts.
     *
     * @param districts the districts
     */
    public void setDistricts(Map<String, CovidApiData> districts) {
        this.districts = districts;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CovidApiStateData.class.getSimpleName() + "[", "]")
                .add("districts=" + districts)
                .add("delta=" + delta)
                .add("total=" + total)
                .add("metaData=" + metaData)
                .toString();
    }
}
